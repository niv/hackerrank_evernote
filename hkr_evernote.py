"""
	Implementation of Evernote engine.
	Commands: CREATE, DELETE, UPDATE, SEARCH.

	Created by Niv B.
	
"""
from sys import stdin

from datetime import datetime, date, time

import xml.dom.minidom

import re # regex

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

#============================================================#

#-- classes

class Note:
	'Note class contains all the data extracted from the note XML, and the XML itself (for now)'
	_CONTENT_TAG_SIZE = 9
	guid = 0 # unique ID
	created = "Uninitialized date"
	created_short = "" # a short representation of the date that will be used for searching, which is given to us in a short format of "yyyymmdd"
	tags = [] # list of all the tags the note has
	content = "" # plain text of the content of the note, used for the search basically (no sufisticated data structure implemented... yet.)
	#xml = "" # the full xml. not really being used
	xml_root = None # used to extract data from it. not used afterwards, but having it as object variable for future implementation needs
	
	xmltags = {"guid":guid , "created":created} # tags and their appropriate name in this class

	def __init__(self, xml):
		xml = self.extractContent(xml) # remove content from the XML, extracting it into self.content
		self.xml_root = ET.fromstring(xml)# parse the XML into an object
		self.setData() # set all the other data for the note object
		searchable[self.guid] = self # adding the note into the searchable database. thanks to python, this works also for update, so we basically do the same for creation and update	
	
	# Removes the <content> tags and content itself from the given XML, return the XML without them.
	# Doing this to avoid invalid XML within the content. assuming no other content XML tags within.
	def extractContent(self, xml):	
		# one way to handle the special characters - i manually search for <content> and take what inside it as string, and parse the rest of the xml regulary. i removed the "content" from the xmltags dictionary
		content_start = xml.find("<content>")
		content_end = xml.find( "</content>", content_start+Note._CONTENT_TAG_SIZE) # searching from index content_start+size of  content tag, because we know it'll be only after the opening tag
		self.content = xml[content_start+Note._CONTENT_TAG_SIZE:content_end] # taking the part between the tags
		return xml[0:content_start] + xml[content_end+Note._CONTENT_TAG_SIZE+1:] # taking the part between the tags out of the XML string so we wont parse it, beause its not a valid XML and will give us errors
	
	# set the tags and created
	def setData(self):
		# search for tags 
		for xml_tag in self.xmltags:
			class_tag_name = self.xml_root.find(xml_tag) # find the given tag name as a class variable
			setattr(self, xml_tag, class_tag_name.text) # initialize that class variable as the text in the XML tag
				
		# add all the tags into the tags data structure
		self.tags = [] # in case we update, we want to lose all previous tags
		for note_tag_key in self.xml_root.findall("tag"):
			self.tags.append(note_tag_key.text)
			
		# making short date for the search	based on date
		self.created_short = self.created[:10].translate(None, '-')

#============================================================#

#-- data structure

# all the searchable notes will be in this list. this is the main data structure
searchable = {}

# dictionary representing type of search available
TYPE_TAG 				= 0
TYPE_CREATED		= 1
TYPE_CONTENT		= 2

search_type_convertion =  {"tag":TYPE_TAG, "created":TYPE_CREATED}


#============================================================#

# - help methods

# return search string and search type ("tag", "created", or "" for normal text) from given input
def extract_search(input):
	# check what type of search it is
	colon_char = input.find(':')
	if colon_char == -1:
		search_string = input #.lower()
		search_type = TYPE_CONTENT 
	else: # in the form of "x:y"
		search_type = search_type_convertion[input[:colon_char]]
		search_string = input[colon_char+1:]
		
	# check for the optional "*" character indicating wildcard at the end of the word. this will allow it to be ONLY at the end of the word. thank u python and regex
	if search_string[-1]=='*':
		search_string = search_string[:-1]+".*"
	
	return (search_type,	search_string)
	
	
# search a single term (word and type) within the GUID list and return all the GUIDs that have been found
def search_single(guid_list, type, search_string):
	
	search_results = []

	# compile the pattern we want to search for 
	if type == TYPE_CONTENT: # search by content is different because of special characters that need to be handled
		pattern = re.compile(r'\b%s[\!\ \.\,\?\n\:\;]' % search_string, re.I)
	else:
		pattern = re.compile(r'\b%s\b' % search_string, re.I)
		
	
	for id in guid_list:
		# get the note from dictionary using the GUID as key
		note = searchable[id]
		
		# search according to type
		
		#----------- search by tag --------------#
		if type == TYPE_TAG:
			for note_xml_tag in note.tags: # going over all the note tags
				if re.search(pattern, note_xml_tag) != None:
					search_results.append(id)
					break # if we found this note has this tag, no point searching its other tags!
					
		#----------- search by date --------------#			
		elif type == TYPE_CREATED:
			if note.created_short >= search_string:
				search_results.append(id)
				
		#----------- search by contend text --------------#			
		else: # normal text search
			if re.search(pattern, note.content) != None:
				search_results.append(id)
	
	return search_results
			
#============================================================#


#-- methods

# create a new note
def op_create(input):
	Note(input)

# update note with new data (same as create thanks to how python handles dictionary/hash )
def op_update(input):
	Note(input)
	
# delete note from searchables
def op_delete(input):
	del searchable[input]

# print a string of the GUIDs delimited by "," that contains the search term(s)
def op_search(input): 
	guid_list = searchable.keys()
	
	# extract and create search term list from input
	for single_string in input.split(' '):
		# extracting the type of our search and the actual string to search
		search_type, search_string  = extract_search(single_string)
		# reducing the GUID list that we gonna search within
		guid_list = search_single(guid_list, search_type, search_string)
		# if we get empty list then we found nothing
		if guid_list == []:
			print ""
			return # no point continue-ing the search, stop now, not before printing the empty line as requested in the documentation
		
	# now sort search results by date
	guid_list = sorted(guid_list, key=lambda id: searchable[id].created);	# we order them by their real date AND time
	
	
	# print the results seperated by comma
	print  ",".join(guid_list)
	
	
	
#============================================================#

# operations allowed, and their respective function
operations = {	"CREATE" : op_create , "UPDATE" : op_update , "DELETE" : op_delete, "SEARCH" : op_search}

				
#============================================================#
#============================================================#

#-- input and engine 

def main():
	    while True :
			try:
				command = raw_input()
			except (EOFError):
				break
			my_input = raw_input()
			#if my_input=="<note>":
			note_index = my_input.find("<note>")
			if my_input.find("<note>") != -1:
				new_input_line = raw_input()
				while new_input_line != "</note>":
					my_input += new_input_line
					new_input_line = raw_input()
				my_input += new_input_line
			operations[command](my_input)

if __name__ == "__main__":
    main()
	
